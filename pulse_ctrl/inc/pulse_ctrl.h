/**
 * @file     pulse_ctrl.h
 * @brief    pulse control header file
 * @date     2024-03-14
 */

/**
 \mainpage pulse control library
 *
 * Introduction
 * ------------
 *
 * 这是一个控制单片机脉冲发送的库
 */

#ifndef __PULSE_CTRL_H__
#define __PULSE_CTRL_H__

#include <pulse_operations.h>
#include <stddef.h>

typedef struct {
  uint32_t id;           /*!< 脉冲设备唯一标识 */
  pulse_operations *ops; /*!< 脉冲设备的操作接口 */
  void *private_data;    /*!< 脉冲设备私有数据*/
} pulse_dev;

pulse_dev *get_pulse_dev(uint32_t id);
bool_t pulse_dev_register(pulse_dev *dev);
inline bool_t generate_number_of_pulse(pulse_dev *dev, uint32_t num,
                                       uint32_t freq, uint32_t duty);
inline bool_t preload_generate_number_of_pulse(pulse_dev *dev, uint32_t num,
                                               uint32_t freq, uint32_t duty);
inline bool_t generate_number_of_pulse_for_custom_data(pulse_dev *dev,
                                                       void *custom_data);

inline pulse_dev_status get_pulse_dev_status(pulse_dev *dev);
inline bool_t pulse_can_preload(pulse_dev *dev);

#endif  // !__PULSE_CTRL_H__
