/**
 * @file     pulse_operations.h
 * @brief    pulse operations header
 * @date     2024-03-14
 */
#ifndef __PULSE_OPERATIONS_H__
#define __PULSE_OPERATIONS_H__

#include <common.h>

typedef enum {
  running,  /*!< 运行中 */
  stop,     /*!< 停止 */
} pulse_dev_status;

typedef struct {
  bool_t (*probe)(void *data); /*!< 设备初始化调用函数 */
  bool_t (*generate_number_of_pulse)(
      void *data, uint32_t num, uint32_t freq,
      uint32_t duty); /*!< 指定频率占空比生成指定数量的脉冲 */
  bool_t (*generate_number_of_pulse_for_custom_data)(
      void *data,
      void *custom_data); /*!< 预加载下一个指定频率占空比生成指定数量的脉冲
                           */
  bool_t (*preload_generate_number_of_pulse)(
      void *data, uint32_t num, uint32_t freq,
      uint32_t duty); /*!< 预加载下一个指定频率占空比生成指定数量的脉冲 */
  bool_t (*can_preload)(void *data);              /*!< 是否可以预加载 */
  pulse_dev_status (*get_dev_status)(void *data); /*!< 获取脉冲设备状态 */
} pulse_operations;

#endif  // !__PULSE_OPERATIONS_H__
