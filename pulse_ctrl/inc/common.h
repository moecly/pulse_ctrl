/**
 * @file     common.h
 * @brief    common header
 * @date     2024-03-14
 */
#ifndef __COMMON_H__
#define __COMMON_H__

typedef unsigned int uint32_t;   /*!< 定义uint32_t */
typedef unsigned short uint16_t; /*!< 定义uint16_t */
typedef unsigned char uint8_t;   /*!< 定于uint8_t */

typedef enum {
  b_false = 0, /*!< 失败 */
  b_true,      /*!< 成功 */
} bool_t;

#endif  // !__COMMON_H__
