/**
 * @file     pulse_ctrl.c
 * @brief    pulse control source
 * @date     2024-03-14
 */
#include <pulse_ctrl.h>

#define MAX_PULSE_DEV 48 /*!< 最大脉冲设备 */
static pulse_dev pulse_dev_list[MAX_PULSE_DEV];
static uint32_t dev_idx = 0;

/**
 * @brief    获取脉冲设备通过设备id
 *
 * @param    id         设备id
 * @return   pulse_dev* 脉冲设备, 找不到设备返回NULL
 */
pulse_dev *get_pulse_dev(uint32_t id) {
  uint32_t i = 0;

  for (; i < dev_idx; i++)
    if (id == pulse_dev_list[i].id) return &pulse_dev_list[i];

  return NULL;
}

/**
 * @brief    注册脉冲设备
 *
 * @param    dev    脉冲设备
 * @return   bool_t 成功返回true, 失败返回false
 *
 * @details  允许注册会初始化脉冲设备
 */
bool_t pulse_dev_register(pulse_dev *dev) {
  if (!dev || !dev->ops) return b_false;

  if (get_pulse_dev(dev->id)) return b_false;

  pulse_dev_list[dev_idx].id = dev->id;
  pulse_dev_list[dev_idx].ops = dev->ops;
  if (dev->ops->probe) return dev->ops->probe(dev->private_data);
  return b_true;
}

/**
 * @brief    指定频率占空比生成指定数量脉冲
 *
 * @param    dev    脉冲设备
 * @param    num    脉冲数
 * @param    freq   脉冲频率
 * @param    duty   脉冲占空比
 * @return   bool_t 成功返回true, 失败返回false
 */
bool_t generate_number_of_pulse(pulse_dev *dev, uint32_t num, uint32_t freq,
                                uint32_t duty) {
  return dev->ops->generate_number_of_pulse(dev->private_data, num, freq, duty);
}

bool_t generate_number_of_pulse_for_custom_data(pulse_dev *dev,
                                                void *custom_data) {
  return dev->ops->generate_number_of_pulse_for_custom_data(dev->private_data,
                                                            custom_data);
}

/**
 * @brief    预加载下一个指定频率占空比生成指定数量的脉冲
 *
 * @param    dev    脉冲设备
 * @param    num    脉冲数
 * @param    freq   脉冲频率
 * @param    duty   脉冲占空比
 * @return   bool_t 成功返回true, 失败返回false
 *
 * @details  如果当前已经脉冲已经发送完成, 则直接开始发送,
 *           否则等待发送完成后再加载
 */
bool_t preload_generate_number_of_pulse(pulse_dev *dev, uint32_t num,
                                        uint32_t freq, uint32_t duty) {
  return dev->ops->preload_generate_number_of_pulse(dev->private_data, num,
                                                    freq, duty);
}

/**
 * @brief    脉冲设备是否可以预加载
 *
 * @param    dev    脉冲设备
 * @return   bool_t true可以预加载, false不可预加载
 */
bool_t pulse_can_preload(pulse_dev *dev) {
  return dev->ops->can_preload(dev->private_data);
}

/**
 * @brief    获取脉冲设备状态
 *
 * @param    dev              脉冲设备
 * @return   pulse_dev_status 脉冲设备状态
 */
pulse_dev_status get_pulse_dev_status(pulse_dev *dev) {
  return dev->ops->get_dev_status(dev->private_data);
}
