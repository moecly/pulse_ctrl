var searchData=
[
  ['preload_5fgenerate_5fnumber_5fof_5fpulse_0',['preload_generate_number_of_pulse',['../pulse__ctrl_8h.html#a73bd4a20f2ff7f61a3276e73598c9b2c',1,'preload_generate_number_of_pulse(pulse_dev *dev, uint32_t num, uint32_t freq, uint32_t duty):&#160;pulse_ctrl.c'],['../pulse__ctrl_8c.html#a73bd4a20f2ff7f61a3276e73598c9b2c',1,'preload_generate_number_of_pulse(pulse_dev *dev, uint32_t num, uint32_t freq, uint32_t duty):&#160;pulse_ctrl.c']]],
  ['pulse_5fcan_5fpreload_1',['pulse_can_preload',['../pulse__ctrl_8h.html#ac3c3196d943f45ae39686d3bf79b9910',1,'pulse_can_preload(pulse_dev *dev):&#160;pulse_ctrl.c'],['../pulse__ctrl_8c.html#ac3c3196d943f45ae39686d3bf79b9910',1,'pulse_can_preload(pulse_dev *dev):&#160;pulse_ctrl.c']]],
  ['pulse_5fdev_5fregister_2',['pulse_dev_register',['../pulse__ctrl_8h.html#a6cda6d2032242ce81dc3f884cba948d4',1,'pulse_dev_register(pulse_dev *dev):&#160;pulse_ctrl.c'],['../pulse__ctrl_8c.html#a6cda6d2032242ce81dc3f884cba948d4',1,'pulse_dev_register(pulse_dev *dev):&#160;pulse_ctrl.c']]]
];
