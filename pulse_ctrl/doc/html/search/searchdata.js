var indexSectionsWithContent =
{
  0: "bcfgilmoprstu",
  1: "p",
  2: "cp",
  3: "gp",
  4: "cgiop",
  5: "u",
  6: "bp",
  7: "frst",
  8: "m",
  9: "clp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "全部",
  1: "类",
  2: "文件",
  3: "函数",
  4: "变量",
  5: "类型定义",
  6: "枚举",
  7: "枚举值",
  8: "宏定义",
  9: "页"
};

