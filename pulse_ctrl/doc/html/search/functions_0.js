var searchData=
[
  ['generate_5fnumber_5fof_5fpulse_0',['generate_number_of_pulse',['../pulse__ctrl_8h.html#a9049d3ea85d91ee5d2bad925ad23f335',1,'generate_number_of_pulse(pulse_dev *dev, uint32_t num, uint32_t freq, uint32_t duty):&#160;pulse_ctrl.c'],['../pulse__ctrl_8c.html#a9049d3ea85d91ee5d2bad925ad23f335',1,'generate_number_of_pulse(pulse_dev *dev, uint32_t num, uint32_t freq, uint32_t duty):&#160;pulse_ctrl.c']]],
  ['get_5fpulse_5fdev_1',['get_pulse_dev',['../pulse__ctrl_8h.html#a015dc99412fb806e585966304965508c',1,'get_pulse_dev(uint32_t id):&#160;pulse_ctrl.c'],['../pulse__ctrl_8c.html#a015dc99412fb806e585966304965508c',1,'get_pulse_dev(uint32_t id):&#160;pulse_ctrl.c']]],
  ['get_5fpulse_5fdev_5fstatus_2',['get_pulse_dev_status',['../pulse__ctrl_8h.html#a7d9bc3d76ae9650cee2011012f347266',1,'get_pulse_dev_status(pulse_dev *dev):&#160;pulse_ctrl.c'],['../pulse__ctrl_8c.html#a7d9bc3d76ae9650cee2011012f347266',1,'get_pulse_dev_status(pulse_dev *dev):&#160;pulse_ctrl.c']]]
];
