/**
 * @file     main.c
 * @brief
 * @date     2024-03-14
 */
#include <NuMicro.h>
#include <mbox_whc.h>
#include <openamp.h>
#include <rpmsg.h>
#include <stdio.h>

#include "debug.h"
#include "led.h"
#include "openamp_operations.h"
#include "pulse.h"

#define PLL_CLOCK 192000000

/**
 * @brief
 */
void sys_init(void) {
  SYS_LockReg();
  /* Enable HXT clock (external XTAL 12MHz) */
  CLK_EnableXtalRC(CLK_PWRCTL_HXTEN_Msk);
  /* Wait for HXT clock ready */
  CLK_WaitClockReady(CLK_STATUS_HXTSTB_Msk);
  /* Set core clock as PLL_CLOCK from PLL */
  CLK_SetCoreClock(PLL_CLOCK);
  SYS_UnlockReg();
}

static void debug_test(void) {
  int ch;
  if (kbhit()) return;
  ch = getchar();
  switch (ch) {
    case 'f': {
      uint32_t i, j, k;
      multiplex_pulse_data d;
      printf("pulse send\n");
      for (k = 0; k < 30; k++) {
        for (j = 1; j <= 20; j++) {
          for (i = 0; i < 4; i++) {
            d.data[i].duty = 50;
            d.data[i].num = j * 1;
          }
          pulse_send(&d);
        }
      }
    } break;
    // case 'd': {
    //   int i;
    //   printf("dma test\n");
    //   for (i = 0; i < 3; i++) {
    //     EPWM0->CLKPSC[i] = 0;
    //     printf("EPWM0->CLKPSC[%d] = %d\n", i, EPWM0->CLKPSC[i]);
    //   }
    //   PDMA_SetTransferCnt(PDMA2, 2, PDMA_WIDTH_32, 3);
    //   PDMA_SetTransferMode(PDMA2, 2, PDMA_MEM, FALSE, 0);
    //   PDMA_Trigger(PDMA2, 2);
    //   for (i = 0; i < 3; i++) {
    //     printf("EPWM0->CLKPSC[%d] = %d\n", i, EPWM0->CLKPSC[i]);
    //   }
    // } break;
    case 'F': {
      uint32_t i, k;
      multiplex_pulse_data d;
      printf("pulse test\n");
      for (k = 0; k < 10; k++) {
        for (i = 0; i < 4; i++) {
          d.data[i].duty = 50;
          d.data[i].num = 1;
        }
        d.data[0].num = 0;
        pulse_send(&d);
      }
    } break;
    case 'r':
      printf("reset\n");
      NVIC_SystemReset();
      break;
    case 'p':
      printf("stop\n");
      EPWM_Stop(EPWM0, 0x1);
      break;
      // case 't':
      //   printf("t\n");
      //   EPWM_ForceStop(EPWM0, 0x1);
      //   printf("%d\n", EPWM_ConfigOutputChannel(EPWM0, 0, 6000000, 50));
      //   EPWM_EnableAcc(EPWM0, 0, 999, EPWM_IFA_PERIOD_POINT);
      //   EPWM_Start(EPWM0, 0x1);
      //   break;
      // case 'T':
      //   printf("T\n");
      //   EPWM_ForceStop(EPWM0, 0x1);
      //   EPWM_ConfigOutputChannel(EPWM0, 0, 100000, 50);
      //   EPWM_EnableAcc(EPWM0, 0, 0, EPWM_IFA_PERIOD_POINT);
      //   EPWM_Start(EPWM0, 0x1);
      //   break;
  }
}

static void servo_test(void) {}

static int rx_callback(struct rpmsg_endpoint *rp_chnl, void *data, size_t len,
                       uint32_t src, void *priv) {
  static int ack = -1;
  int command;
  multiplex_pulse_data info;

  if (ack == -1) {
    memcpy((void *)(&ack), (const void *)src, sizeof(int));
    return 0;
  }

  switch (ack) {
    case MSG: {
      memcpy((void *)(&info), (const void *)src, sizeof(multiplex_pulse_data));
      pulse_send(&info);
      ack = -1;
    } break;

    case CMD:
      /*
       * 判断是什么命令
       */
      memcpy((void *)&command, (const void *)src, sizeof(int));
      if (command == START_CMD) {
        PM2 = 1;
        PM3 = 1;
        PM4 = 1;
        PM5 = 1;
        PM6 = 1;
        PM7 = 1;
        PM8 = 1;
        PM9 = 1;
        servo_test();
      }
      if (command == STOP_CMD) {
        PM2 = 0;
        PM3 = 0;
        PM4 = 0;
        PM5 = 0;
        PM6 = 0;
        PM7 = 0;
        PM8 = 0;
        PM9 = 0;
        servo_test();
      }
      ack = -1;
      break;
  }

  return 0;
}

int main(void) {
  sys_init();
  debug_init();
  printf("running...\n");
  openamp_init("rpmsg-sample", 0xFFFFFFFF, rx_callback, NULL);
  led_init();
  pulse_init(1000);
  printf("run completed\n");
  while (1) {
    /* 检查rpmsg是否有数据 */
    int k, i;
    multiplex_pulse_data d;
    openamp_check_for_message();
    debug_test();
    // for (k = 0; k < 10; k++) {
    //   for (i = 0; i < 4; i++) {
    //     d.data[i].duty = 50;
    //     d.data[i].num = 500;
    //   }
    //   // d.data[0].num = 0;
    //   pulse_send(&d);
    // }
  }
}
