#ifndef __OPENAMP_OPERATIONS_H__
#define __OPENAMP_OPERATIONS_H__

#include "NuMicro.h"
// #include "mbox_whc.h"
#include <stdio.h>

#include "openamp.h"
#include "rpmsg.h"

#define MSG 0
#define CMD 1
#define START_CMD 1
#define STOP_CMD 0
#define M4_COMMAND_ACK 0x81
#define TX_RX_SIZE 256

void openamp_init(const char *name, uint32_t dest, rpmsg_ept_cb cb,
                  rpmsg_ns_unbind_cb unbindCb);

inline void openamp_check_for_message(void);

#endif  // !__OPENAMP_OPERATIONS_H__
