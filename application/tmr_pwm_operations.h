/**
 * @file     tmr_pwm_operations.h
 * @brief    tmr pwm operations header
 * @date     2024-03-14
 */
#ifndef __TMR_PWM_OPERATIONS_H__
#define __TMR_PWM_OPERATIONS_H__

#include <NuMicro.h>
#include <pulse_operations.h>

#define PRELOAD_BUFFER_LEN 20

typedef enum {
  tmr_pwm_independent_mode,   /*!< 独立模式 */
  tmr_pwm_complementary_mode, /*!< 互补模式 */
} tmr_pwm_mode;

typedef struct {
  uint32_t prescaler;
  uint32_t period;
  uint32_t cmp;
  uint32_t num;
} tmr_pwm_reg;

typedef struct {
  // bool_t is_preload; /*!< 预加载标志位 */
  // uint32_t num;      /*!< 预加载脉冲数 */
  // uint32_t freq;     /*!< 预加载频率 */
  // uint32_t duty;     /*!< 预加载占空比 */
  uint32_t left;
  uint32_t right;
  tmr_pwm_reg reg[PRELOAD_BUFFER_LEN];
} tmr_pwm_preload;

typedef struct {
  TIMER_T *master;         /*!< 主定时器 */
  TIMER_T *slave;          /*!< 从定时器 */
  tmr_pwm_mode mode;       /*!< pwm模式 */
  IRQn_Type slave_irq;     /*!< 从定时器中断号 */
  pulse_dev_status status; /*!< pwm状态 */
  tmr_pwm_preload preload; /*!< 用于预加载脉冲 */
} tmr_pwm_info;

void get_tmr_pwm_operations(pulse_operations **ops);

/**
 * @brief    注册中断回调函数
 * @param    func_name 中断函数名
 * @param    master    主定时器
 * @param    slave     从定时器
 */
#define tmr_pwm_irq_handler_register(func_name, info)                 \
  void func_name(void) {                                              \
    tmr_pwm_reg *reg = &info.preload.reg[info.preload.left];          \
    TIMER_ClearIntFlag(info.slave);                                   \
    TPWM_SET_CMPDAT(info.master, 0);                                  \
    TPWM_SET_PERIOD(info.master, 0);                                  \
    if (info.preload.left == info.preload.right || reg->num == 0) {   \
      info.status = stop;                                             \
      return;                                                         \
    }                                                                 \
    info.preload.left = (info.preload.left + 1) % PRELOAD_BUFFER_LEN; \
    TIMER_SET_CMP_VALUE(info.slave, reg->num);                        \
    TPWM_SET_PRESCALER(info.master, reg->prescaler);                  \
    TIMER_Start(info.slave);                                          \
    TPWM_SET_CMPDAT(info.master, reg->cmp);                           \
    TPWM_SET_PERIOD(info.master, reg->period);                        \
  }

#endif  // !__TMR_PWM_OPERATIONS_H__
