#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <NuMicro.h>
#include <common.h>

bool_t debug_init(void);
int kbhit(void);

#endif  // !__DEBUG_H__
