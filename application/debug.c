#include "debug.h"

static void sys_init(void) {
  SYS_LockReg();
  /* Enable UART module clock */
  CLK_EnableModuleClock(UART10_MODULE);
  CLK_SetModuleClock(UART10_MODULE, CLK_CLKSEL3_UART10SEL_HXT,
                     CLK_CLKDIV2_UART10(1));
  SYS->GPH_MFPL &= ~(SYS_GPH_MFPL_PH6MFP_Msk | SYS_GPH_MFPL_PH7MFP_Msk);
  SYS->GPH_MFPL |=
      (SYS_GPH_MFPL_PH6MFP_UART10_RXD | SYS_GPH_MFPL_PH7MFP_UART10_TXD);
  SYS_UnlockReg();
}

bool_t debug_init(void) {
  sys_init();
  UART_Open(UART10, 115200);
  return b_true;
}
