/**
 * @file     epwm_operations.h
 * @brief    epwm operations header
 * @date     2024-03-15
 */
#ifndef __EPWM__OPERATIONS_H__
#define __EPWM__OPERATIONS_H__

#include <NuMicro.h>
#include <pulse_operations.h>

#define PRELOAD_BUFFER_LEN 20

typedef struct {
  uint32_t prescaler;
  uint32_t cnr;
  uint32_t cmr;
  uint32_t num;
} epwm_reg;

typedef struct {
  uint32_t left;
  uint32_t right;
  epwm_reg reg[PRELOAD_BUFFER_LEN];
} epwm_preload;

typedef struct {
  EPWM_T *dev;
  uint32_t ch;
  uint32_t ch_mask;
  IRQn_Type epwm_irq;
  pulse_dev_status status;
  epwm_preload preload;
} epwm_info;

void get_epwm_operations(pulse_operations **ops);

// #define epwm_irq_handler_register(func_name, info) \
//   void func_name(void) {                           \
//     EPWM_ClearAccInt(info.dev, info.ch);           \
//     EPWM_SET_CMR(info.dev, info.ch, 0);            \
//     EPWM_DisableAcc(info.dev, info.ch);            \
//     info.status = stop;                            \
//   }

// EPWM_DisableAcc(info.dev, info.ch);                 \
    EPWM_DisableAcc(info.dev, info.ch);            \

// while (info.dev->CNT[info.ch] < info.dev->CMPDAT[info.ch]) { \
      // }                                                            \

#define epwm_irq_handler_register(func_name, info) \
  void func_name(void) {                           \
    EPWM_ClearAccInt(info.dev, info.ch);           \
    info.status = stop;                            \
  }

// printf("info.status: %d\n", info.status);      \

// EPWM_EnableAcc(info.dev, info.ch, 0xffff, EPWM_IFA_PERIOD_POINT); \
// EPWM_DisableAcc(info.dev, info.ch);            \
// EPWM_Start(info.dev, info.ch_mask);                                     \
// EPWM_SET_CMR(info.dev, info.ch, 0);                                     \
// EPWM_ForceStop(info.dev, info.ch_mask);                           \

// #define epwm_irq_handler_register(func_name, info)                        \
//   void func_name(void) {                                                  \
//     epwm_reg *reg = &info.preload.reg[info.preload.left];                 \
//     EPWM_ClearAccInt(info.dev, info.ch);                                  \
//     if ((info.preload.left == info.preload.right) || reg->num == 0) {     \
//       EPWM_SET_CMR(info.dev, info.ch, 0);                                 \
//       EPWM_EnableAcc(info.dev, info.ch, 0, EPWM_IFA_ZERO_POINT);          \
//       info.status = stop;                                                 \
//       return;                                                             \
//     }                                                                     \
//     info.preload.left = (info.preload.left + 1) % PRELOAD_BUFFER_LEN;     \
//     EPWM_SET_PRESCALER(info.dev, info.ch, reg->prescaler);                \
//     EPWM_SET_CNR(info.dev, info.ch, reg->cnr);                            \
//     EPWM_SET_CMR(info.dev, info.ch, reg->cmr);                            \
//     EPWM_EnableAcc(info.dev, info.ch, reg->num - 1, EPWM_IFA_ZERO_POINT); \
//   }

// #define epwm_irq_handler_register(func_name, info)                    \
//   void func_name(void) {                                              \
//     epwm_reg *reg = &info.preload.reg[info.preload.left];             \
//     EPWM_ClearAccInt(info.dev, info.ch);                              \
//     EPWM_SET_CMR(info.dev, info.ch, 0);                               \
//     if ((info.preload.left == info.preload.right) || reg->num == 0) { \
//       info.dev->IFA[info.ch] &= ~((uint32_t)1 << 31);                 \
//       info.status = stop;                                             \
//       return;                                                         \
//     }                                                                 \
//     info.preload.left = (info.preload.left + 1) % PRELOAD_BUFFER_LEN; \
//     info.dev->IFA[info.ch] = ((reg->num - 1) & 0xffff);               \
//     EPWM_SET_PRESCALER(info.dev, info.ch, reg->prescaler);            \
//     EPWM_SET_CNR(info.dev, info.ch, reg->cnr);                        \
//     info.dev->IFA[info.ch] |= ((uint32_t)1 << 31);                    \
//     EPWM_SET_CMR(info.dev, info.ch, reg->cmr);                        \
//   }

// info.dev->IFA[info.ch] &= ~(0xffff);                            \
// EPWM_SET_CMR(info.dev, ch, 0);                                  \
// info.dev->CNTEN &= ~(1 << ch);                                         \
// EPWM_DisableAcc(info.dev, ch);                                    \

#endif  // !__EPWM__OPERATIONS_H__
