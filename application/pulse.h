/**
 * @file     pulse.h
 * @brief    pulse header
 * @date     2024-03-14
 */
#ifndef __PULSE_H__
#define __PULSE_H__

#include <common.h>

#include "epwm_operations.h"

#define PULSE_DATA_LEN 1000
#define PULSE_DEV_NUM 4

typedef struct {
  uint32_t num;  /*!< 脉冲数据 */
  uint32_t freq; /*!< 脉冲频率 */
  uint32_t duty; /*!< 脉冲占空比 */
} pulse_data;

typedef struct {
  pulse_data data[PULSE_DEV_NUM];
} multiplex_pulse_data;

typedef struct {
  multiplex_pulse_data data[PULSE_DATA_LEN]; /*!< 脉冲数据数组 */
  uint32_t left;                             /*!< 循环数组左索引 */
  uint32_t right;                            /*!< 循环数组右索引 */
} pulse_data_arr;

bool_t pulse_init(uint32_t cycle);
// bool_t pulse_send(uint32_t dev, uint32_t num, uint32_t duty);
bool_t pulse_send(multiplex_pulse_data *data);
void pulse_test(void);

#endif  // !__PULSE_H__
