/**
 * @file     tmr_pwm_operations.c
 * @brief    tmr pwm operations source
 * @date     2024-03-14
 */
#include <tmr_pwm_operations.h>

static pulse_operations pulse_ops;

#define next_pos(x) \
  ((x + 1) % PRELOAD_BUFFER_LEN) /*!< 获取环形数组索引下一个坐标值 */

/**
 * @brief    数组是否装满
 *
 * @param    reg    预加载脉冲数组
 * @return   bool_t true已装满, false未装满
 */
static bool_t is_full(tmr_pwm_preload *reg) {
  return reg->left == next_pos(reg->right) ? b_true : b_false;
}

/**
 * @brief    数组是否为空
 *
 * @param    reg    预加载脉冲数组
 * @return   bool_t true为空, false不为空
 */
static bool_t is_empty(tmr_pwm_preload *reg) {
  return reg->left == reg->right ? b_true : b_false;
}

/**
 * @brief    插入数据
 *
 * @param    reg    预加载脉冲数组
 * @param    data   脉冲数据
 * @return   bool_t true成功, false失败
 */
static bool_t push_data(tmr_pwm_preload *reg, tmr_pwm_reg data) {
  if (is_full(reg)) return b_false;
  reg->reg[reg->right] = data;
  reg->right = next_pos(reg->right);
  return b_true;
}

/**
 * @brief    弹出数据
 *
 * @param    reg    预加载脉冲数组
 * @param    data   脉冲数据
 * @return   bool_t true成功, false失败
 */
static bool_t pop_data(tmr_pwm_preload *reg, tmr_pwm_reg *data) {
  if (is_empty(reg)) return b_false;
  *data = reg->reg[reg->left];
  reg->left = next_pos(reg->left);
  return b_true;
}

/**
 * @brief    计算pwm寄存器配置值
 *
 * @param    freq        pwm频率
 * @param    duty        pwm占空比
 * @param    num         pwm数量
 * @return   tmr_pwm_reg pwm寄存器配置
 */
static tmr_pwm_reg cal_pwm_reg(uint32_t freq, uint32_t duty, uint32_t num) {
  uint32_t u32PWMClockFreq;
  uint32_t u32Prescaler = 0x1000UL, u32Period, u32CMP;
  tmr_pwm_reg ret;

  u32PWMClockFreq = CLK_GetSYSCLK1Freq();

  /* Calculate u16PERIOD and u16PSC */
  for (u32Prescaler = 1UL; u32Prescaler <= 0x1000UL; u32Prescaler++) {
    u32Period = (u32PWMClockFreq / u32Prescaler) / freq;

    /* If target u32Period is larger than 0x10000, need to use a larger
     * prescaler */
    if (u32Period <= 0x10000UL) {
      break;
    }
  }

  /* Convert to real register value */
  ret.prescaler = u32Prescaler - 1UL;

  ret.period = u32Period - 1UL;

  if (duty) {
    u32CMP = (duty * u32Period) / 100UL;
  } else {
    u32CMP = 0UL;
  }

  ret.cmp = u32CMP;
  ret.num = num;
  return ret;
}

/**
 * @brief    设备初始化调用函数
 *
 * @param    data   设备私有数据, tmr_pwm_info *获取
 * @return   bool_t 成功返回true, 失败返回false
 */
static bool_t tmr_pwm_probe(void *data) {
  tmr_pwm_info *pwm_info = (tmr_pwm_info *)data;

  pwm_info->status = stop;
  pwm_info->preload.left = 0;
  pwm_info->preload.right = 0;

  /* 初始化从定时器 */
  TIMER_Open(pwm_info->slave, TIMER_ONESHOT_MODE, 100);
  TIMER_SET_PRESCALE_VALUE(pwm_info->slave, 0);
  TIMER_EnableEventCounter(pwm_info->slave, TIMER_COUNTER_EVENT_FALLING);
  TIMER_EnableInt(pwm_info->slave);
  NVIC_SetPriority(pwm_info->slave_irq, 0);
  NVIC_EnableIRQ(pwm_info->slave_irq);

  /* 初始化主定时器 */
  TPWM_ENABLE_PWM_MODE(pwm_info->master);
  TPWM_ENABLE_OUTPUT(pwm_info->master, TPWM_CH0 | TPWM_CH1);
  TPWM_SET_COUNTER_TYPE(pwm_info->master, TPWM_UP_COUNT);
  switch (pwm_info->mode) {
    case tmr_pwm_independent_mode:
      TPWM_ENABLE_INDEPENDENT_MODE(pwm_info->master);
      break;
    case tmr_pwm_complementary_mode:
      TPWM_ENABLE_COMPLEMENTARY_MODE(pwm_info->master);
      break;
  }

  /* Set PWM to auto-reload mode */
  pwm_info->master->PWMCTL =
      (pwm_info->master->PWMCTL & ~TIMER_PWMCTL_CNTMODE_Msk) |
      TPWM_AUTO_RELOAD_MODE;
  TPWM_START_COUNTER(pwm_info->master);

  return b_true;
}

/**
 * @brief    指定频率生成指定数量的脉冲
 *
 * @param    data   设备私有数据, tmr_pwm_info *获取
 * @param    num    脉冲数量
 * @param    freq   脉冲频率
 * @param    duty   脉冲占空比
 * @return   bool_t 成功返回true, 失败返回false
 */
static bool_t tmr_pwm_generate_number_of_pulse(void *data, uint32_t num,
                                               uint32_t freq, uint32_t duty) {
  tmr_pwm_info *pwm_info = (tmr_pwm_info *)data;
  if (num == 0) return b_false;
  pwm_info->status = running;
  TIMER_SET_CMP_VALUE(pwm_info->slave, num);
  TPWM_ConfigOutputFreqAndDuty(pwm_info->master, freq, duty);
  TIMER_Start(pwm_info->slave);
  return b_true;
}

/**
 * @brief    获取脉冲设备状态
 *
 * @param    data             设备私有数据, tmr_pwm_info *获取
 * @return   pulse_dev_status 设备状态
 */
static pulse_dev_status tmr_pwm_get_dev_status(void *data) {
  tmr_pwm_info *pwm_info = (tmr_pwm_info *)data;
  return pwm_info->status;
}

/**
 * @brief    预加载下一个指定频率占空比生成指定数量的脉冲
 *
 * @param    data   设备私有数据, tmr_pwm_info *获取
 * @param    num    脉冲数量
 * @param    freq   脉冲频率
 * @param    duty   脉冲占空比
 * @return   bool_t 成功返回true, 失败返回false
 *
 * @details  如果当前已经脉冲已经发送完成, 则直接开始发送,
 *           否则等待发送完成后再加载
 */
static bool_t tmr_pwm_preload_generate_number_of_pulse(void *data, uint32_t num,
                                                       uint32_t freq,
                                                       uint32_t duty) {
  tmr_pwm_info *pwm_info = (tmr_pwm_info *)data;
  tmr_pwm_reg reg;
  if (num == 0) return b_false;
  if (!push_data(&pwm_info->preload, cal_pwm_reg(freq, duty, num)))
    return b_false;
  if (pwm_info->status == stop) {
    if (!pop_data(&pwm_info->preload, &reg)) return b_false;
    pwm_info->status = running;
    (pwm_info->master)->PWMCTL &= ~TIMER_PWMCTL_CNTEN_Msk;
    TIMER_SET_CMP_VALUE(pwm_info->slave, reg.num);
    TPWM_SET_PRESCALER(pwm_info->master, reg.prescaler);
    TPWM_SET_CMPDAT(pwm_info->master, reg.cmp);
    TPWM_SET_PERIOD(pwm_info->master, reg.period);
    TIMER_Start(pwm_info->slave);
    (pwm_info->master)->PWMCTL |= TIMER_PWMCTL_CNTEN_Msk;
  }
  return b_true;
}

/**
 * @brief    是否已经预加载
 *
 * @param    data   设备私有数据, tmr_pwm_info *获取
 * @return   bool_t true已经预加载, false还未预加载
 */
static bool_t tmr_pwm_can_preload(void *data) {
  tmr_pwm_info *pwm_info = (tmr_pwm_info *)data;
  return is_full(&pwm_info->preload) ? b_false : b_true;
}

/**
 * @brief    获取主从定时器的脉冲设备操作
 *
 * @param    ops 脉冲操作
 */
void get_tmr_pwm_operations(pulse_operations **ops) {
  pulse_ops.probe = tmr_pwm_probe;
  pulse_ops.generate_number_of_pulse = tmr_pwm_generate_number_of_pulse;
  pulse_ops.get_dev_status = tmr_pwm_get_dev_status;
  pulse_ops.preload_generate_number_of_pulse =
      tmr_pwm_preload_generate_number_of_pulse;
  pulse_ops.can_preload = tmr_pwm_can_preload;
  *ops = &pulse_ops;
}
