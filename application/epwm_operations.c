/**
 * @file     epwm_operations.c
 * @brief    epwm operations source
 * @date     2024-03-15
 */
#include "epwm_operations.h"

static pulse_operations pulse_ops;

#define next_pos(x) \
  ((x + 1) % PRELOAD_BUFFER_LEN) /*!< 获取环形数组索引下一个坐标值 */

/**
 * @brief    数组是否装满
 *
 * @param    reg    预加载脉冲数组
 * @return   bool_t true已装满, false未装满
 */
static bool_t is_full(epwm_preload *reg) {
  return reg->left == next_pos(reg->right) ? b_true : b_false;
}

/**
 * @brief    数组是否为空
 *
 * @param    reg    预加载脉冲数组
 * @return   bool_t true为空, false不为空
 */
static bool_t is_empty(epwm_preload *reg) {
  return reg->left == reg->right ? b_true : b_false;
}

/**
 * @brief    插入数据
 *
 * @param    reg    预加载脉冲数组
 * @param    data   脉冲数据
 * @return   bool_t true成功, false失败
 */
static bool_t push_data(epwm_preload *reg, epwm_reg data) {
  if (is_full(reg)) return b_false;
  reg->reg[reg->right] = data;
  reg->right = next_pos(reg->right);
  return b_true;
}

/**
 * @brief    弹出数据
 *
 * @param    reg    预加载脉冲数组
 * @param    data   脉冲数据
 * @return   bool_t true成功, false失败
 */
static bool_t pop_data(epwm_preload *reg, epwm_reg *data) {
  if (is_empty(reg)) return b_false;
  *data = reg->reg[reg->left];
  reg->left = next_pos(reg->left);
  return b_true;
}

static epwm_reg cal_epwm_reg(uint32_t freq, uint32_t duty, uint32_t num) {
  epwm_reg reg;
  uint32_t u32EPWMClockSrc;
  uint32_t i;
  uint32_t u32Prescale = 1U, u32CNR = 0xFFFFU;

  /* clock source is from PCLK */
  SystemCoreClockUpdate();
  u32EPWMClockSrc = CLK_GetPCLK0Freq();

  for (u32Prescale = 1U; u32Prescale < 0xFFFU;
       u32Prescale++) /* prescale could be 0~0xFFF */
  {
    i = (u32EPWMClockSrc / freq) / u32Prescale;
    /* If target value is larger than CNR, need to use a larger prescaler */
    if (i < (0x10000U)) {
      u32CNR = i;
      break;
    }
  }
  /* Store return value here 'cos we're gonna change u16Prescale & u16CNR to the
   * real value to fill into register */
  i = u32EPWMClockSrc / (u32Prescale * u32CNR);

  /* convert to real register value */
  /* every two channels share a prescaler */
  u32Prescale -= 1U;
  reg.prescaler = u32Prescale;

  if (u32CNR > 0) u32CNR -= 1U;
  reg.cnr = u32CNR;
  reg.cmr = duty * (u32CNR + 1U) / 100U;
  reg.num = num;
  return reg;
}

/**
 * @brief    设备初始化调用函数
 *
 * @param    data   设备私有数据, epwm_info *获取
 * @return   bool_t 成功返回true, 失败返回false
 */
static bool_t epwm_probe(void *data) {
  epwm_info *info = (epwm_info *)data;

  info->status = stop;
  info->preload.left = 0;
  info->preload.right = 0;

  EPWM_DISABLE_COMPLEMENTARY_MODE(info->dev);
  info->ch_mask = (1 << info->ch);
  EPWM_EnableAccStopMode(info->dev, info->ch);
  EPWM_ForceStop(info->dev, info->ch_mask);
  EPWM_EnableAccInt(info->dev, info->ch);
  EPWM_EnableAcc(info->dev, info->ch, 0, EPWM_IFA_ZERO_POINT);
  EPWM_DisableAcc(info->dev, info->ch);
  EPWM_DisableAccPDMA(info->dev, info->ch);
  (info->dev)->CTL1 =
      ((info->dev)->CTL1 & ~((EPWM_CTL1_CNTTYPE0_Msk << (info->ch << 1U)) |
                             ((1UL << EPWM_CTL1_CNTMODE0_Pos) << info->ch)));
  EPWM_SET_OUTPUT_LEVEL(info->dev, 1 << info->ch, EPWM_OUTPUT_HIGH,
                        EPWM_OUTPUT_LOW, EPWM_OUTPUT_LOW, EPWM_OUTPUT_LOW);
  EPWM_SET_CMR(info->dev, 0, 0);
  EPWM_SET_CNR(info->dev, 0, 0);
  EPWM_EnableOutput(info->dev, info->ch_mask);
  EPWM_ENABLE_TIMER_SYNC(info->dev, info->ch_mask, EPWM_SSCTL_SSRC_EPWM0);
  NVIC_SetPriority(info->epwm_irq, 0);
  NVIC_EnableIRQ(info->epwm_irq);

  return b_true;
}

/**
 * @brief    获取脉冲设备状态
 *
 * @param    data             设备私有数据, epwm_info *获取
 * @return   pulse_dev_status 设备状态
 */
static pulse_dev_status epwm_get_dev_status(void *data) {
  epwm_info *info = (epwm_info *)data;
  // if ((info->dev->CNTEN & info->ch_mask) == 0) return stop;
  return info->status;
}

/**
 * @brief    指定频率生成指定数量的脉冲
 *
 * @param    data   设备私有数据, epwm_info *获取
 * @param    num    脉冲数量
 * @param    freq   脉冲频率
 * @param    duty   脉冲占空比
 * @return   bool_t 成功返回true, 失败返回false
 */
static bool_t epwm_generate_number_of_pulse(void *data, uint32_t num,
                                            uint32_t freq, uint32_t duty) {
  epwm_info *info = (epwm_info *)data;
  epwm_reg reg;
  if (num == 0) return b_true;
  reg = cal_epwm_reg(freq, duty, num);
  info->status = running;
  EPWM_ForceStop(info->dev, info->ch_mask);
  EPWM_SET_PRESCALER(info->dev, info->ch, reg.prescaler);
  EPWM_SET_CNR(info->dev, info->ch, reg.cnr);
  EPWM_SET_CMR(info->dev, info->ch, reg.cmr);
  EPWM_EnableAcc(info->dev, info->ch, reg.num - 1, EPWM_IFA_PERIOD_POINT);
  return b_true;
}

/**
 * @brief    指定频率生成指定数量的脉冲
 *
 * @param    data   设备私有数据, epwm_info *获取
 * @param    num    脉冲数量
 * @param    freq   脉冲频率
 * @param    duty   脉冲占空比
 * @return   bool_t 成功返回true, 失败返回false
 */
static bool_t epwm_generate_number_of_pulse_for_custom_data(void *data,
                                                            void *custom_data) {
  epwm_info *info = (epwm_info *)data;
  epwm_reg *reg = (epwm_reg *)custom_data;
  info->status = running;
  if (reg->num == 0) {
    EPWM_EnableAcc(info->dev, info->ch, 0, EPWM_IFA_ZERO_POINT);
    return b_true;
  }
  EPWM_SET_PRESCALER(info->dev, info->ch, reg->prescaler);
  EPWM_SET_CNR(info->dev, info->ch, reg->cnr);
  EPWM_SET_CMR(info->dev, info->ch, reg->cmr);
  EPWM_EnableAcc(info->dev, info->ch, reg->num - 1, EPWM_IFA_ZERO_POINT);
  return b_true;
}

/**
 * @brief    预加载下一个指定频率占空比生成指定数量的脉冲
 *
 * @param    data   设备私有数据, epwm_info *获取
 * @param    num    脉冲数量
 * @param    freq   脉冲频率
 * @param    duty   脉冲占空比
 * @return   bool_t 成功返回true, 失败返回false
 *
 * @details  如果当前已经脉冲已经发送完成, 则直接开始发送,
 *           否则等待发送完成后再加载
 */
static bool_t epwm_preload_generate_number_of_pulse(void *data, uint32_t num,
                                                    uint32_t freq,
                                                    uint32_t duty) {
  epwm_info *info = (epwm_info *)data;
  epwm_reg reg;

  if (num == 0) {
    // EPWM_EnableAcc(info->dev, info->ch, 0, EPWM_IFA_ZERO_POINT);
    return b_true;
  }
  if (!push_data(&info->preload, cal_epwm_reg(freq, duty, num))) return b_false;
  if (info->status == stop) {
    if (!pop_data(&info->preload, &reg)) return b_false;
    info->status = running;
    EPWM_SET_PRESCALER(info->dev, info->ch, reg.prescaler);
    EPWM_SET_CNR(info->dev, info->ch, reg.cnr);
    EPWM_SET_CMR(info->dev, info->ch, reg.cmr);
    EPWM_EnableAcc(info->dev, info->ch, reg.num - 1, EPWM_IFA_ZERO_POINT);
  }
  return b_true;
}

/**
 * @brief    是否已经预加载
 *
 * @param    data   设备私有数据, epwm_info *获取
 * @return   bool_t true已经预加载, false还未预加载
 */
static bool_t epwm_can_preload(void *data) {
  epwm_info *pwm_info = (epwm_info *)data;
  return is_full(&pwm_info->preload) ? b_false : b_true;
}

/**
 * @brief    获取epwm的脉冲设备操作
 *
 * @param    ops 脉冲操作
 */
void get_epwm_operations(pulse_operations **ops) {
  pulse_ops.probe = epwm_probe;
  pulse_ops.generate_number_of_pulse = epwm_generate_number_of_pulse;
  pulse_ops.get_dev_status = epwm_get_dev_status;
  pulse_ops.preload_generate_number_of_pulse =
      epwm_preload_generate_number_of_pulse;
  pulse_ops.generate_number_of_pulse_for_custom_data =
      epwm_generate_number_of_pulse_for_custom_data;
  pulse_ops.can_preload = epwm_can_preload;
  *ops = &pulse_ops;
}
