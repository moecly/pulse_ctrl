var searchData=
[
  ['preload_0',['preload',['../structtmr__pwm__info.html#a848228b90f8453f8028fd240ed05ad69',1,'tmr_pwm_info']]],
  ['pulse_2ec_1',['pulse.c',['../pulse_8c.html',1,'']]],
  ['pulse_2eh_2',['pulse.h',['../pulse_8h.html',1,'']]],
  ['pulse_5fdata_3',['pulse_data',['../structpulse__data.html',1,'']]],
  ['pulse_5fdata_5farr_4',['pulse_data_arr',['../structpulse__data__arr.html',1,'']]],
  ['pulse_5finit_5',['pulse_init',['../pulse_8c.html#ad5a2824244b78649b43153e795f0dcc9',1,'pulse_init(uint32_t cycle):&#160;pulse.c'],['../pulse_8h.html#ad5a2824244b78649b43153e795f0dcc9',1,'pulse_init(uint32_t cycle):&#160;pulse.c']]],
  ['pulse_5fsend_6',['pulse_send',['../pulse_8c.html#a010bb0659b79eab45f274ac39e19249d',1,'pulse_send(uint32_t dev, uint32_t num, uint32_t duty):&#160;pulse.c'],['../pulse_8h.html#a010bb0659b79eab45f274ac39e19249d',1,'pulse_send(uint32_t dev, uint32_t num, uint32_t duty):&#160;pulse.c']]]
];
