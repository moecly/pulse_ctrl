#include "openamp_operations.h"

static struct rpmsg_endpoint rpmsg_ept;

/**
 * @brief: 初始化RPMsg远程设备与本地设备之间的OpenAMP通信。
 * @params:
 *      - name: 端点名称，用于标识和通信。
 *      - dest: 与远程设备通信的目标地址。
 *      - cb: 用于处理从远程设备接收的消息的回调函数。
 *      - unbindCb: 用于处理解绑定的回调函数。
 */
void openamp_init(const char *name, uint32_t dest, rpmsg_ept_cb cb,
                  rpmsg_ns_unbind_cb unbindCb) {
  MA35D1_OpenAMP_Init(RPMSG_REMOTE, NULL);
  OPENAMP_create_endpoint(&rpmsg_ept, name, dest, cb, unbindCb);
}

/**
 * @brief: 检查是否有来自RPMsg远程设备的消息。
 * @note: 该函数会检查当前的OpenAMP通信端点是否有来自远程设备的消息，
 *        并触发相应的处理函数来处理接收到的消息。
 */
void openamp_check_for_message(void) { OPENAMP_check_for_message(&rpmsg_ept); }
