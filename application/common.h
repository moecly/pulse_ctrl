#ifndef __COMMON_H__
#define __COMMON_H__

typedef unsigned int uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char uint8_t;

// typedef int int32_t;
// typedef short int16_t;
// typedef char int8_t;

typedef enum {
  b_false = 0,
  b_true,
} bool_t;

#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif

#endif  // !__COMMON_H__
