/**
 * @file     pulse.c
 * @brief    pulse source
 * @date     2024-03-14
 */
#include "pulse.h"

#include <pulse_ctrl.h>
#include <string.h>

#include "epwm_operations.h"
#include "openamp_operations.h"
#include "tmr_pwm_operations.h"

static uint32_t pulse_cycle = 0;
static pulse_dev p_dev[PULSE_DEV_NUM];
static pulse_data_arr p_data;
// static tmr_pwm_info tpwm_info[2];
static volatile epwm_info ep_info[PULSE_DEV_NUM];

// dma
static uint32_t pre[3][3] = {0};
static uint32_t cmr[3][6] = {0};
static uint32_t cnr[3][6] = {0};
static uint32_t ifa[3][6] = {0};

#define next_pos(x) \
  ((x + 1) % PULSE_DATA_LEN) /*!< 获取环形数组索引下一个坐标值 */

static void dma_trigger(void);

/**
 * @brief    数组是否装满
 *
 * @param    pulse  脉冲数组
 * @return   bool_t true已装满, false未装满
 */
static inline bool_t is_full(pulse_data_arr *pulse) {
  return pulse->left == next_pos(pulse->right) ? b_true : b_false;
}

/**
 * @brief    数组是否为空
 *
 * @param    pulse  脉冲数组
 * @return   bool_t true为空, false不为空
 */
static inline bool_t is_empty(pulse_data_arr *pulse) {
  return pulse->left == pulse->right ? b_true : b_false;
}

/**
 * @brief    插入数据
 *
 * @param    pulse  脉冲数组
 * @param    data   脉冲数据
 * @return   bool_t true成功, false失败
 */
static inline bool_t push_data(pulse_data_arr *pulse,
                               multiplex_pulse_data data) {
  if (is_full(pulse)) return b_false;
  pulse->data[pulse->right] = data;
  pulse->right = next_pos(pulse->right);
  return b_true;
}

/**
 * @brief    弹出数据
 *
 * @param    pulse  脉冲数组
 * @param    data   脉冲数据
 * @return   bool_t true成功, false失败
 */
static inline bool_t pop_data(pulse_data_arr *pulse,
                              multiplex_pulse_data *data) {
  if (is_empty(pulse)) return b_false;
  *data = pulse->data[pulse->left];
  pulse->left = next_pos(pulse->left);
  return b_true;
}

/**
 * @brief    计算脉冲频率
 *
 * @param    pulse_num 脉冲数量
 * @param    time_us   脉冲周期
 * @return   uint32_t  脉冲频率
 */
static uint32_t cal_frequency(uint32_t pulse_num, uint32_t time_us) {
  uint32_t one_sec = 1000000;
  double multiple = (double)one_sec / time_us;
  return (uint32_t)(pulse_num * multiple);
}

/**
 * @brief    pwm模块初始化
 */
static void pwm_sys_init(void) {
  /* Unlock protected registers */
  SYS_UnlockReg();

  /* tmr pwm模块时钟 */
  CLK_EnableModuleClock(TMR2_MODULE);
  CLK_EnableModuleClock(TMR4_MODULE);
  CLK_EnableModuleClock(TMR6_MODULE);
  CLK_EnableModuleClock(TMR7_MODULE);

  /* gpio模块时钟 */
  CLK_EnableModuleClock(GPL_MODULE);
  CLK_EnableModuleClock(GPB_MODULE);
  CLK_EnableModuleClock(GPN_MODULE);
  CLK_EnableModuleClock(GPG_MODULE);
  CLK_EnableModuleClock(GPM_MODULE);

  /* epwm模块时钟 */
  CLK_EnableModuleClock(EPWM0_MODULE);
  CLK_EnableModuleClock(EPWM1_MODULE);
  CLK_EnableModuleClock(EPWM2_MODULE);

  /* dma模块时钟 */
  CLK_EnableModuleClock(PDMA2_MODULE);
  CLK_EnableModuleClock(PDMA3_MODULE);

  /* 脉冲周期定时器时钟 */
  CLK_EnableModuleClock(TMR3_MODULE);
  CLK_SetModuleClock(TMR3_MODULE, CLK_CLKSEL1_TMR3SEL_PCLK1, 0);

  /* tmr pwm时钟源 */
  CLK_SetModuleClock(TMR2_MODULE, CLK_CLKSEL1_TMR2SEL_PCLK1, 0);
  CLK_SetModuleClock(TMR4_MODULE, CLK_CLKSEL1_TMR4SEL_PCLK2, 0);
  CLK_SetModuleClock(TMR6_MODULE, CLK_CLKSEL1_TMR6SEL_PCLK0, 0);
  CLK_SetModuleClock(TMR7_MODULE, CLK_CLKSEL1_TMR7SEL_PCLK0, 0);

  /* Update System Core Clock */
  /* User can use SystemCoreClockUpdate() to calculate SystemCoreClock. */
  SystemCoreClockUpdate();

  /* 设置tmr pwm输入引脚复用 */
  SYS->GPL_MFPH &= ~(SYS_GPL_MFPH_PL14MFP_Msk);
  SYS->GPL_MFPH |= SYS_GPL_MFPH_PL14MFP_TM2;
  SYS->GPB_MFPH &= ~(SYS_GPB_MFPH_PB8MFP_Msk);
  SYS->GPB_MFPH |= SYS_GPB_MFPH_PB8MFP_TM4;
  SYS->GPN_MFPH &= ~(SYS_GPN_MFPH_PN15MFP_Msk);
  SYS->GPN_MFPH |= SYS_GPN_MFPH_PN15MFP_TM6;
  SYS->GPG_MFPL &= ~(SYS_GPG_MFPL_PG6MFP_Msk);
  SYS->GPG_MFPL |= SYS_GPG_MFPL_PG6MFP_TM7;

  /* 设置epwm输入引脚复用 */
  SYS->GPG_MFPL &= ~(SYS_GPG_MFPL_PG0MFP_Msk | SYS_GPG_MFPL_PG4MFP_Msk |
                     SYS_GPG_MFPL_PG7MFP_Msk);
  SYS->GPG_MFPL |=
      (SYS_GPG_MFPL_PG0MFP_EPWM0_CH0 | SYS_GPG_MFPL_PG4MFP_EPWM1_CH0 |
       SYS_GPG_MFPL_PG7MFP_EPWM1_CH3);
  SYS->GPB_MFPH &= ~(SYS_GPB_MFPH_PB9MFP_Msk);
  SYS->GPB_MFPH |= SYS_GPB_MFPH_PB9MFP_EPWM2_CH4;

  /* epwm方向引脚0-7轴 */
  SYS->GPM_MFPL &= ~(SYS_GPM_MFPL_PM2MFP_Msk | SYS_GPM_MFPL_PM3MFP_Msk |
                     SYS_GPM_MFPL_PM4MFP_Msk | SYS_GPM_MFPL_PM5MFP_Msk |
                     SYS_GPM_MFPL_PM6MFP_Msk | SYS_GPM_MFPL_PM7MFP_Msk);
  SYS->GPM_MFPL |= (SYS_GPM_MFPL_PM2MFP_GPIO | SYS_GPM_MFPL_PM3MFP_GPIO |
                    SYS_GPM_MFPL_PM4MFP_GPIO | SYS_GPM_MFPL_PM5MFP_GPIO |
                    SYS_GPM_MFPL_PM6MFP_GPIO | SYS_GPM_MFPL_PM7MFP_GPIO);
  SYS->GPM_MFPH &= ~(SYS_GPM_MFPH_PM8MFP_Msk | SYS_GPM_MFPH_PM9MFP_Msk);
  SYS->GPM_MFPH |= (SYS_GPM_MFPH_PM8MFP_GPIO | SYS_GPM_MFPH_PM9MFP_GPIO);

  /* Lock protected registers */
  SYS_LockReg();
}

static void dma_init0(PDMA_T *dma, uint32_t ch, uint32_t tran_len, uint32_t src,
                      uint32_t dst, IRQn_Type irq) {
  PDMA_Open(dma, (1 << ch));

  /* Transfer width is half word(16 bit) and transfer count is 1 */
  PDMA_SetTransferCnt(dma, ch, PDMA_WIDTH_32, tran_len);

  /* Set source address as g_u32Count array(increment) and destination address
   * as EPWM1 channel 0 period register(no increment) */
  PDMA_SetTransferAddr(dma, ch, src, PDMA_SAR_INC, dst, PDMA_DAR_INC);

  /* Select PDMA request source as PDMA_EPWM1_CH0_TX(EPWM1 channel 0 accumulator
   * interrupt) */
  PDMA_SetTransferMode(dma, ch, PDMA_MEM, FALSE, 0);

  /* Set PDMA as single request type for EPWM */
  PDMA_SetBurstType(dma, ch, PDMA_REQ_BURST, PDMA_BURST_2);

  PDMA_EnableInt(dma, ch, PDMA_INT_TRANS_DONE);
  // NVIC_EnableIRQ(irq);
  NVIC_DisableIRQ(irq);
}

static void dma_init(void) {
  dma_init0(PDMA2, 0, 3, (uint32_t)&pre[0], (uint32_t) & (EPWM0->CLKPSC[0]),
            PDMA2_IRQn);
  dma_init0(PDMA2, 1, 6, (uint32_t)&cmr[0], (uint32_t) & (EPWM0->CMPDAT[0]),
            PDMA2_IRQn);
  dma_init0(PDMA2, 2, 6, (uint32_t)&cnr[0], (uint32_t) & (EPWM0->PERIOD[0]),
            PDMA2_IRQn);
  dma_init0(PDMA2, 3, 6, (uint32_t)&ifa[0], (uint32_t) & (EPWM0->IFA[0]),
            PDMA2_IRQn);
  dma_init0(PDMA2, 4, 3, (uint32_t)&pre[1], (uint32_t) & (EPWM1->CLKPSC[0]),
            PDMA2_IRQn);
  dma_init0(PDMA2, 5, 6, (uint32_t)&cmr[1], (uint32_t) & (EPWM1->CMPDAT[0]),
            PDMA2_IRQn);
  dma_init0(PDMA3, 0, 6, (uint32_t)&cnr[1], (uint32_t) & (EPWM1->PERIOD[0]),
            PDMA3_IRQn);
  dma_init0(PDMA3, 1, 6, (uint32_t)&ifa[1], (uint32_t) & (EPWM1->IFA[0]),
            PDMA3_IRQn);
  dma_init0(PDMA3, 2, 3, (uint32_t)&pre[2], (uint32_t) & (EPWM2->CLKPSC[0]),
            PDMA3_IRQn);
  dma_init0(PDMA3, 3, 6, (uint32_t)&cmr[2], (uint32_t) & (EPWM2->CMPDAT[0]),
            PDMA3_IRQn);
  dma_init0(PDMA3, 4, 6, (uint32_t)&cnr[2], (uint32_t) & (EPWM2->PERIOD[0]),
            PDMA3_IRQn);
  dma_init0(PDMA3, 5, 6, (uint32_t)&ifa[2], (uint32_t) & (EPWM2->IFA[0]),
            PDMA3_IRQn);
}

static epwm_reg cal_epwm_reg(uint32_t freq, uint32_t duty, uint32_t num) {
  epwm_reg reg;
  uint32_t u32EPWMClockSrc;
  uint32_t i;
  uint32_t u32Prescale = 1U, u32CNR = 0xFFFFU;

  /* clock source is from PCLK */
  SystemCoreClockUpdate();
  u32EPWMClockSrc = CLK_GetPCLK0Freq();

  for (u32Prescale = 1U; u32Prescale < 0xFFFU;
       u32Prescale++) /* prescale could be 0~0xFFF */
  {
    i = (u32EPWMClockSrc / freq) / u32Prescale;
    /* If target value is larger than CNR, need to use a larger prescaler */
    if (i < (0x10000U)) {
      u32CNR = i;
      break;
    }
  }
  /* Store return value here 'cos we're gonna change u16Prescale & u16CNR to the
   * real value to fill into register */
  i = u32EPWMClockSrc / (u32Prescale * u32CNR);

  /* convert to real register value */
  /* every two channels share a prescaler */
  u32Prescale -= 1U;
  reg.prescaler = u32Prescale;

  if (u32CNR > 0) u32CNR -= 1U;
  reg.cnr = u32CNR;
  reg.cmr = duty * (u32CNR + 1U) / 100U;
  reg.num = num;
  return reg;
}

/* 主从定时器中断回调注册 */
// tmr_pwm_irq_handler_register(TMR7_IRQHandler, tpwm_info[0]);
// tmr_pwm_irq_handler_register(TMR4_IRQHandler, tpwm_info[1]);
epwm_irq_handler_register(EPWM0P0_IRQHandler, ep_info[0]);
epwm_irq_handler_register(EPWM2P2_IRQHandle, ep_info[1]);
epwm_irq_handler_register(EPWM1P1_IRQHandler, ep_info[2]);
epwm_irq_handler_register(EPWM1P0_IRQHandler, ep_info[3]);
// epwm_irq_handler_register(EPWM0P1_IRQHandler, ep_info[4]);
// epwm_irq_handler_register(EPWM0P2_IRQHandler, ep_info[5]);
// epwm_irq_handler_register(EPWM1P3_IRQHandler, ep_info[6]);
// epwm_irq_handler_register(EPWM2P0_IRQHandle, ep_info[7]);
// epwm_irq_handler_register(EPWM2P1_IRQHandle, ep_info[8]);

/**
 * @brief       PDMA IRQ Handler
 *
 * @param       None
 *
 * @return      None
 *
 * @details     ISR to handle PDMA interrupt event
 */
void PDMA2_IRQHandler(void) {
  uint32_t status = PDMA_GET_INT_STATUS(PDMA2);

  if (PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF0_Msk)
    PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF0_Msk);
  if (PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF1_Msk)
    PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF1_Msk);
  if (PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF2_Msk)
    PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF2_Msk);
  if (PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF3_Msk)
    PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF3_Msk);
  if (PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF4_Msk)
    PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF4_Msk);
  if (PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF5_Msk)
    PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF5_Msk);
  if (PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF6_Msk)
    PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF6_Msk);
  if (PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF7_Msk)
    PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF7_Msk);
}

void PDMA3_IRQHandler(void) {
  uint32_t status = PDMA_GET_INT_STATUS(PDMA3);

  if (PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF0_Msk)
    PDMA_CLR_TD_FLAG(PDMA3, PDMA_TDSTS_TDIF0_Msk);
  if (PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF1_Msk)
    PDMA_CLR_TD_FLAG(PDMA3, PDMA_TDSTS_TDIF1_Msk);
  if (PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF2_Msk)
    PDMA_CLR_TD_FLAG(PDMA3, PDMA_TDSTS_TDIF2_Msk);
  if (PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF3_Msk)
    PDMA_CLR_TD_FLAG(PDMA3, PDMA_TDSTS_TDIF3_Msk);
}

static void dma_trigger(void) {
  PDMA2->SWREQ = 0x37;
  PDMA3->SWREQ = 0x1d;
  PDMA2->SWREQ = 0x8;
  PDMA3->SWREQ = 0x2;
  PDMA3->SWREQ = 0x20;
}

static void reset_dma(void) {
  while ((PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF0_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF0_Msk);
  while ((PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF1_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF1_Msk);
  while ((PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF2_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF2_Msk);
  while ((PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF3_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF3_Msk);
  while ((PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF4_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF4_Msk);
  while ((PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF5_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA2, PDMA_TDSTS_TDIF5_Msk);
  while ((PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF0_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA3, PDMA_TDSTS_TDIF0_Msk);
  while ((PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF1_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA3, PDMA_TDSTS_TDIF1_Msk);
  while ((PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF2_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA3, PDMA_TDSTS_TDIF2_Msk);
  while ((PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF3_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA3, PDMA_TDSTS_TDIF3_Msk);
  while ((PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF4_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA3, PDMA_TDSTS_TDIF4_Msk);
  while ((PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF5_Msk) == 0) {
  }
  PDMA_CLR_TD_FLAG(PDMA3, PDMA_TDSTS_TDIF5_Msk);

  PDMA_SetTransferCnt(PDMA2, 0, PDMA_WIDTH_32, 3);
  PDMA_SetTransferMode(PDMA2, 0, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA2, 1, PDMA_WIDTH_32, 6);
  PDMA_SetTransferMode(PDMA2, 1, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA2, 2, PDMA_WIDTH_32, 6);
  PDMA_SetTransferMode(PDMA2, 2, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA2, 3, PDMA_WIDTH_32, 6);
  PDMA_SetTransferMode(PDMA2, 3, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA2, 4, PDMA_WIDTH_32, 3);
  PDMA_SetTransferMode(PDMA2, 4, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA2, 5, PDMA_WIDTH_32, 6);
  PDMA_SetTransferMode(PDMA2, 5, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA3, 0, PDMA_WIDTH_32, 6);
  PDMA_SetTransferMode(PDMA3, 0, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA3, 1, PDMA_WIDTH_32, 6);
  PDMA_SetTransferMode(PDMA3, 1, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA3, 2, PDMA_WIDTH_32, 3);
  PDMA_SetTransferMode(PDMA3, 2, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA3, 3, PDMA_WIDTH_32, 6);
  PDMA_SetTransferMode(PDMA3, 3, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA3, 4, PDMA_WIDTH_32, 6);
  PDMA_SetTransferMode(PDMA3, 4, PDMA_MEM, FALSE, 0);
  PDMA_SetTransferCnt(PDMA3, 5, PDMA_WIDTH_32, 6);
  PDMA_SetTransferMode(PDMA3, 5, PDMA_MEM, FALSE, 0);
}

/**
 * @brief    脉冲周期回调函数
 *
 * @details  加载脉冲数据
 */
void TMR3_IRQHandler(void) {
  uint32_t i = 0;
  uint32_t dev = 0, ch = 0;
  multiplex_pulse_data d;
  epwm_reg reg[PULSE_DEV_NUM];
  static epwm_reg last_reg[PULSE_DEV_NUM] = {0};
  static volatile epwm_info *running_dev = NULL;
  TIMER_ClearIntFlag(TIMER3);
#if 0
  if (is_empty(&p_data)) return;

  for (; i < PULSE_DEV_NUM; i++)
    if (!pulse_can_preload(&p_dev[i])) return;

  if (!pop_data(&p_data, &d)) return;

  for (i = 0; i < PULSE_DEV_NUM; i++)
    preload_generate_number_of_pulse(&p_dev[i], d.data[i].num, d.data[i].freq,
                                     d.data[i].duty);

    // while (!is_empty(&p_data)) {
    //   for (i = 0; i < PULSE_DEV_NUM; i++)
    //     if (!pulse_can_preload(&p_dev[i])) return;
    //   for (i = 0; i < PULSE_DEV_NUM; i++)
    //     preload_generate_number_of_pulse(&p_dev[i], d.data[i].num,
    //     d.data[i].freq,
    //                                      d.data[i].duty);
    // }
#elif 0
  if (!pop_data(&p_data, &d)) return;

  for (; i < PULSE_DEV_NUM; i++) {
    reg[i] = cal_epwm_reg(d.data[i].freq, d.data[i].duty, d.data[i].num);
    if (reg[i].num != 0) flag = 1;
  }

  if (flag == 0) return;

  for (i = 0; i < PULSE_DEV_NUM; i++) {
    // while (get_pulse_dev_status(&p_dev[i]) == running) {
    // }
    generate_number_of_pulse_for_custom_data(&p_dev[i], (void *)&reg[i]);
  }

  for (i = 0; i < PULSE_DEV_NUM; i++) {
    while ((ep_info[i].dev->CNTEN & ep_info[i].ch_mask) == 0x1) {
    }
  }

  EPWM_TRIGGER_SYNC_START(EPWM0);
#elif 0
  if (!pop_data(&p_data, &d)) return;

  for (i = 0; i < PULSE_DEV_NUM; i++)
    preload_generate_number_of_pulse(&p_dev[i], d.data[i].num, d.data[i].freq,
                                     d.data[i].duty);

  for (i = 0; i < PULSE_DEV_NUM; i++) {
    while ((ep_info[i].dev->CNTEN & ep_info[i].ch_mask) == 0x1) {
    }
  }

  // while ((ep_info[0].dev->CNTEN & ep_info[0].ch_mask) == 0x1) {
  // }

  EPWM_TRIGGER_SYNC_START(EPWM0);

#else
  if (!pop_data(&p_data, &d)) return;
  for (i = 0; i < PULSE_DEV_NUM; i++) {
    reg[i] = cal_epwm_reg(d.data[i].freq, d.data[i].duty, d.data[i].num);
    if (ep_info[i].dev == EPWM0) {
      dev = 0;
    } else if (ep_info[i].dev == EPWM1) {
      dev = 1;
    } else {
      dev = 2;
    }
    ch = ep_info[i].ch;
    pre[dev][ch / 2] = reg[i].prescaler;
    cmr[dev][ch] = reg[i].cmr;
    cnr[dev][ch] = reg[i].cnr;
    if (reg[i].num >= 1) {
      ifa[dev][ch] = ((reg[i].num - 1) | 0x81000000);
    } else {
      cmr[dev][ch] = 0;
      ifa[dev][ch] = (0x81000000);
    }
  }
  for (i = 0; i < PULSE_DEV_NUM; i++) {
    while (ep_info[i].status == running) {
    }
  }

  for (i = 0; i < PULSE_DEV_NUM; i++) {
    ep_info[i].status = running;
  }

  for (i = 0; i < PULSE_DEV_NUM; i++) {
    if (reg[i].num <= 5)
      while ((ep_info[i].dev->CNTEN & ep_info[i].ch_mask) ==
             ep_info[i].ch_mask) {
      }
    // while (((ep_info[i].dev->CNT[ep_info[i].ch]) >
    //         ep_info[i].dev->CMPDAT[ep_info[i].ch]) &&
    //        ((ep_info[i].dev->CNT[ep_info[i].ch] & 0xffff) != 0)) {
    // }
  }

  dma_trigger();
  for (i = 0; i < PULSE_DEV_NUM; i++) {
    while ((ep_info[i].dev->CNTEN & ep_info[i].ch_mask) == ep_info[i].ch_mask) {
    }
  }

  while ((PDMA_GET_TD_STS(PDMA2) & PDMA_TDSTS_TDIF3_Msk) == 0) {
  }
  while ((PDMA_GET_TD_STS(PDMA3) & PDMA_TDSTS_TDIF5_Msk) == 0) {
  }

  EPWM_TRIGGER_SYNC_START(EPWM0);
  reset_dma();
  memcpy(last_reg, reg, sizeof(epwm_reg) * PULSE_DEV_NUM);
  running_dev = NULL;
  for (i = 0; i < PULSE_DEV_NUM; i++) {
    if (reg[i].num > 0) {
      running_dev = &ep_info[i];
      break;
    }
  }
#endif
}

/**
 * @brief    脉冲发送
 *
 * @param    dev    设备索引
 * @param    num    脉冲数量
 * @param    duty   脉冲占空比
 *
 * @return   bool_t 成功返回true, 失败返回false
 */
bool_t pulse_send(multiplex_pulse_data *data) {
  uint32_t i = 0;
  for (; i < PULSE_DEV_NUM; i++)
    data->data[i].freq = cal_frequency(data->data[i].num, pulse_cycle);
  return push_data(&p_data, *data);
}

/**
 * @brief    脉冲初始化
 * @param    cycle  脉冲周期长度, 单位us
 *
 * @return   bool_t 成功返回true, 失败返回false
 */
bool_t pulse_init(uint32_t cycle) {
  pulse_cycle = cycle;
  pwm_sys_init();
  dma_init();
  GPIO_SetMode(PM, 0x3fc, GPIO_MODE_OUTPUT);

  memset(&p_data, 0, sizeof(p_data));

#if 0
  /* id:0 m:TMR6 s:TMR7 */
  p_dev[0].id = 0;
  tpwm_info[0].master = TIMER6;
  tpwm_info[0].slave = TIMER7;
  tpwm_info[0].mode = tmr_pwm_complementary_mode;
  tpwm_info[0].slave_irq = TMR7_IRQn;
  p_dev[0].private_data = (void *)&tpwm_info[0];
  get_tmr_pwm_operations(&p_dev[0].ops);
  pulse_dev_register(&p_dev[0]); 

  /* id:1 m:TMR2 s:TMR4 */
  p_dev[1].id = 1;
  tpwm_info[1].master = TIMER2;
  tpwm_info[1].slave = TIMER4;
  tpwm_info[1].mode = tmr_pwm_complementary_mode;
  tpwm_info[1].slave_irq = TMR4_IRQn;
  p_dev[1].private_data = (void *)&tpwm_info[1];
  get_tmr_pwm_operations(&p_dev[1].ops);
  pulse_dev_register(&p_dev[1]);

  /* id:2 epwm0_ch0 */
  p_dev[2].id = 2;
  ep_info[0].dev = EPWM0;
  ep_info[0].ch = 0;
  ep_info[0].epwm_irq = EPWM0P0_IRQn;
  p_dev[2].private_data = (void *)&ep_info[0];
  get_epwm_operations(&p_dev[2].ops);
  pulse_dev_register(&p_dev[2]);

  /* id:3 epwm1_ch0 */
  p_dev[3].id = 3;
  ep_info[1].dev = EPWM2;
  ep_info[1].ch = 4;
  ep_info[1].epwm_irq = EPWM2P2_IRQn;
  p_dev[3].private_data = (void *)&ep_info[1];
  get_epwm_operations(&p_dev[3].ops);
  pulse_dev_register(&p_dev[3]);

  /* id:4 epwm1_ch3 */
  p_dev[4].id = 5;
  ep_info[2].dev = EPWM1;
  ep_info[2].ch = 3;
  ep_info[2].epwm_irq = EPWM1P1_IRQn;
  p_dev[4].private_data = (void *)&ep_info[2];
  get_epwm_operations(&p_dev[4].ops);
  pulse_dev_register(&p_dev[4]);

  /* id:3 epwm1_ch0 */
  p_dev[5].id = 3;
  ep_info[3].dev = EPWM1;
  ep_info[3].ch = 0;
  ep_info[3].epwm_irq = EPWM1P0_IRQn;
  p_dev[5].private_data = (void *)&ep_info[3];
  get_epwm_operations(&p_dev[5].ops);
  pulse_dev_register(&p_dev[5]);
#else
  /* id:0 epwm0_ch0 */
  p_dev[0].id = 0;
  ep_info[0].dev = EPWM0;
  ep_info[0].ch = 0;
  ep_info[0].epwm_irq = EPWM0P0_IRQn;
  p_dev[0].private_data = (void *)&ep_info[0];
  get_epwm_operations(&p_dev[0].ops);
  pulse_dev_register(&p_dev[0]);

  /* id:1 epwm1_ch0 */
  p_dev[1].id = 1;
  ep_info[1].dev = EPWM2;
  ep_info[1].ch = 4;
  ep_info[1].epwm_irq = EPWM2P2_IRQn;
  p_dev[1].private_data = (void *)&ep_info[1];
  get_epwm_operations(&p_dev[1].ops);
  pulse_dev_register(&p_dev[1]);

  /* id:2 epwm1_ch3 */
  p_dev[2].id = 2;
  ep_info[2].dev = EPWM1;
  ep_info[2].ch = 3;
  ep_info[2].epwm_irq = EPWM1P1_IRQn;
  p_dev[2].private_data = (void *)&ep_info[2];
  get_epwm_operations(&p_dev[2].ops);
  pulse_dev_register(&p_dev[2]);

  /* id:3 epwm1_ch0 */
  p_dev[3].id = 3;
  ep_info[3].dev = EPWM1;
  ep_info[3].ch = 0;
  ep_info[3].epwm_irq = EPWM1P0_IRQn;
  p_dev[3].private_data = (void *)&ep_info[3];
  get_epwm_operations(&p_dev[3].ops);
  pulse_dev_register(&p_dev[3]);

  // /* id:4 epwm0_ch2 */
  // p_dev[4].id = 4;
  // ep_info[4].dev = EPWM0;
  // ep_info[4].ch = 2;
  // ep_info[4].epwm_irq = EPWM1P0_IRQn;
  // p_dev[4].private_data = (void *)&ep_info[4];
  // get_epwm_operations(&p_dev[4].ops);
  // pulse_dev_register(&p_dev[4]);

  // /* id:5 epwm0_ch2 */
  // p_dev[5].id = 5;
  // ep_info[5].dev = EPWM0;
  // ep_info[5].ch = 4;
  // ep_info[5].epwm_irq = EPWM1P0_IRQn;
  // p_dev[5].private_data = (void *)&ep_info[5];
  // get_epwm_operations(&p_dev[5].ops);
  // pulse_dev_register(&p_dev[5]);

  // /* id:6 epwm1_ch4 */
  // p_dev[6].id = 6;
  // ep_info[6].dev = EPWM1;
  // ep_info[6].ch = 4;
  // ep_info[6].epwm_irq = EPWM1P0_IRQn;
  // p_dev[6].private_data = (void *)&ep_info[6];
  // get_epwm_operations(&p_dev[6].ops);
  // pulse_dev_register(&p_dev[6]);

  // /* id:7 epwm2_ch0 */
  // p_dev[7].id = 7;
  // ep_info[7].dev = EPWM2;
  // ep_info[7].ch = 0;
  // ep_info[7].epwm_irq = EPWM1P0_IRQn;
  // p_dev[7].private_data = (void *)&ep_info[7];
  // get_epwm_operations(&p_dev[7].ops);
  // pulse_dev_register(&p_dev[7]);

  // /* id:8 epwm2_ch2 */
  // p_dev[8].id = 8;
  // ep_info[8].dev = EPWM2;
  // ep_info[8].ch = 2;
  // ep_info[8].epwm_irq = EPWM1P0_IRQn;
  // p_dev[8].private_data = (void *)&ep_info[8];
  // get_epwm_operations(&p_dev[8].ops);
  // pulse_dev_register(&p_dev[8]);
#endif
  TIMER_Open(TIMER3, TIMER_PERIODIC_MODE, 1000000 / (cycle - 100));
  TIMER_EnableInt(TIMER3);
  NVIC_SetPriority(TMR3_IRQn, 1);
  NVIC_EnableIRQ(TMR3_IRQn);
  TIMER_Start(TIMER3);

  return b_true;
}
