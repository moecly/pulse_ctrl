#include <led.h>

static void sys_init(void) {
  /* Unlock protected registers */
  SYS_UnlockReg();

  /* Enable peripheral clock */
  CLK_EnableModuleClock(GPN_MODULE);

  /* Update System Core Clock */
  /* User can use SystemCoreClockUpdate() to calculate SystemCoreClock. */
  SystemCoreClockUpdate();

  /* Set GPIO output pins */
  SYS->GPN_MFPL &= ~(SYS_GPN_MFPL_PN6MFP_Msk | SYS_GPN_MFPL_PN7MFP_Msk);
  SYS->GPN_MFPL |= SYS_GPN_MFPL_PN6MFP_GPIO | SYS_GPN_MFPL_PN7MFP_GPIO;
  SYS->GPN_MFPH &= ~(SYS_GPN_MFPH_PN10MFP_Msk);
  SYS->GPN_MFPH |= SYS_GPN_MFPH_PN10MFP_GPIO;

  /* Lock protected registers */
  SYS_LockReg();
}

void led_init(void) {
  sys_init();
  GPIO_SetMode(PN, BIT6, GPIO_MODE_OUTPUT);
  GPIO_SetMode(PN, BIT7, GPIO_MODE_OUTPUT);
  GPIO_SetMode(PN, BIT10, GPIO_MODE_OUTPUT);
  PN6 = 0;
  PN7 = 0;
  PN10 = 0;
}
