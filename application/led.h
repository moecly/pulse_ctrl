#ifndef __LED_H__
#define __LED_H__

#include <NuMicro.h>

#define led0 PN6
#define led1 PN7
#define led2 PN10

void led_init(void);

#endif  // !__LED_H__
